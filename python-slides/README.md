# Programação para Ciência de Dados em Python

Este repositório contém os notebooks de slides para as aulas de Python. 

Estes slides podem sofrer modificações no decorrer das aulas, como correções de código fonte, erros gramaticais, e possível adição de conteúdo relevante (caso requisitado pelos alunos).

Os notebooks podem ser visualizados na interface do github (bastando acessar os arquivos), ou então visualizados na sua máquina local.

## Entrega dos notebooks

Os notebooks com os exercícios resolvidos serão coletados, **impreterivelmente**, dia **03/08/2018**. 

Os notebooks a serem coletados devem ser os que estão disponibilizados no **seu repositório. Não utilize os notebooks que estão no repositório python-slides, 
pois podem haver diferenças no ID dos boxes, invalidando a correção dos exercícios no autograder.**

### Como será feita a coleta dos notebooks?

Executaremos um script que percorrerá **todos os repositórios dos alunos a meia noite desta data**. Os notebooks serão copiados para a máquina dos professores, e então o autograder será executado. Portanto, **certifique-se de fazer commit para o seu repositório remoto antes da data limite de coleta.** Além disso, você não precisa fazer mais nada. **Não envie seus notebooks por e-mail após esta data, pois os mesmos não serão considerados.**

## Cronograma de aulas

As aulas da disciplina seguirão o seguinte cronograma:

|            | Domingo        | Segunda        | Terça          | Quarta         | Quinta         |     Sexta      | Sábado         |
|:----------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
| Dia        | 01/07/2018     | 02/07/2018     | 03/07/2018     | 04/07/2018     | 05/07/2018     | 06/07/2018     | 07/07/2018     |
| Hora       |                |                |                |                |                | 18h45 -- 22h30 | 09h00 -- 12h30 |
| Professor  |                |                |                |                |                | Felipe         | Felipe         |
| Conteúdo   |                |                |                |                |                | Introdução     | Parte Básica   |
| Dia        | 08/07/2018     | 09/07/2018     | 10/07/2018     | 11/07/2018     | 12/07/2018     | 13/07/2018     | 14/07/2018     |
| Hora       |                |                |                |                |                |                |                |
| Professor  |                |                |                |                |                |                |                |
| Conteúdo   |                |                |                |                |                |                |                |
| Dia        | 15/07/2018     | 16/07/2018     | 17/07/2018     | 18/07/2018     | 19/07/2018     | 20/07/2018     | 21/07/2018     |
| Hora       |                | 18h45 -- 22h30 | 18h45 -- 22h30 | 18h45 -- 22h30 | 18h45 -- 22h30 | 18h45 -- 22h30 |                |
| Professor  |                | Henry          | Henry          | Henry          | Henry          | Henry          |                |
| Conteúdo   |                | Funções        | Contêineres    | Contêineres    | Classes        | Numpy          |                | 
| Dia        | 22/07/2018     | 23/07/2018     | 24/07/2018     | 25/07/2018     | 26/07/2018     | 27/07/2018     | 28/07/2018     |
| Hora       |                | 18h45 -- 22h30 | 18h45 -- 22h30 | 18h45 -- 22h30 | 18h45 -- 22h30 | 18h45 -- 22h30 |                |
| Professor  |                | Henry          | Felipe         | Felipe         | Felipe         | Felipe         |                |
| Conteúdo   |                | Numpy          | Pandas         | Pandas         | Matplotlib     | Matplotlib     |                |

Alterações podem ser realizadas devido ao andamento das aulas.

## Visualização local 

### Criação do ambiente virtual 
Para visualizar os slides, é necessário criar um ambiente virtual no Anaconda:

1. Instale a distribuição Anaconda do Python 3: [link](https://www.anaconda.com/download/#linux)
2. Crie um novo interpretador Python:

```
conda create --name py3 python=3
```
3. Ative o ambiente virtual 

no Linux:
```
source activate py3
```
no Windows:
```
activate py3
```

4. Instale os pacotes necessários usando o arquivo ```requirements.txt```:

```
conda install --file requirements.txt
```

### Visualização dos notebooks (sem html)

1. Ative o ambiente virtual 

no Linux:
```
source activate py3
```
no Windows:
```
activate py3
```
2. Ative o jupyter notebook:

```
jupyter notebook
```

3. Dentro da interface aberta, navegue até a pasta onde o notebook está e abra-o.

### Visualização dos notebooks (em html)

Para gerar slides de contedo das aulas, utilize o seguinte comando (na linha de comando):

```
jupyter nbconvert python_avancado/classes_01_construtores.ipynb --to slides --post serve
```

Isto gerará um arquivo .html na **mesma pasta em que o notebook está**, e abrirá uma nova janela no navegador padrão, com a apresentação de slides. Para sair da apresentação, simplesmente feche a janela no navegador e termine o processo no terminal utilizado para invocar o comando.

**Note que este comando deve sempre ser utilizado para realizar a apresentação de slides. Abrir o arquivo .html não habilitará o modo de apresentação.**

