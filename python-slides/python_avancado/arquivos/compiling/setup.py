from distutils.core import setup, Extension

setup(
	name = 'modelo',  # nome da biblioteca
	version = '0.1a',
	description = 'Pacote demonstrativo de codigo fonte em C',
	ext_modules = [
		Extension(
			'modelo',  # nome do modulo
			sources=['modelo_randomico.c']
		)
	]
)