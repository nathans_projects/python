// Agradecimentos a physacco pelo exemplo: https://gist.github.com/physacco/2e1b52415f3a964ad2a542a99bebed8f

#include <stdlib.h>  // para usar a funcao rand
#include <Python.h>


static PyObject* modelo_randomico(PyObject *self, PyObject *args) {
    /**
     * Funcao interna do modulo. Essa funcao pode ter o formato e documentacao necessarios dentro deste arquivo, ja que 
     * nao sera a documentacao que sera mostrada ao usuario final (que e definida na variavel modelo_randomico_doc).
     */

    int n_objects, n_classes; 
    PyObject *predictions, *data, *shape;
    
    // faz o parsing dos valores passados como parametro para a funcao; retorna um erro caso falte algum parametro
    if (!PyArg_ParseTuple(
            args, "O!O!O!i",
            &PyTuple_Type, &shape,
            &PyList_Type, &data,
            &PyList_Type, &predictions,
            &n_classes)
        ) {
        return NULL;
    }
 
    // coleta o numero de objetos, que esta dentro de uma tupla
    n_objects = (int)PyLong_AsLong(PyTuple_GetItem(shape, 0));
    
    int i;
    for(i = 0; i < n_objects; i++) {
        // atribui uma classe randomica, no intervalo [0, n_classes), para cada objeto
        PyList_SetItem(predictions, (Py_ssize_t)i, Py_BuildValue("i", rand() % n_classes));  
    }

    // retorna as predicoes
    return Py_BuildValue("O", predictions);
}

const char modelo_randomico_doc[] = "Faz predicoes randomicas para um determinado conjunto de dados.\n\n"
    ":param shape: (numero de instancias, numero de atributos)\n"
    ":param dataset: Conjunto de dados sem o atributo classe (i.e. X)\n"
    ":param predictions: Uma lista vazia onde as predicoes serao armazenadas\n"
    ":param n_classes: numero de classes no problema.\n"
    ":returns: Lista de predicoes, uma entrada por linha no dataset.";


// Definicao dos metodos que compoem esta extensao
// Cada entrada nesta lista e uma outra lista, composta de 4 itens:
// ml_name: Nome do metodo (como sera visivel para o usuario final; pode ser diferente do nome da funcao interna deste arquivo);
// ml_meth: Ponteiro para a implementacao do metodo;
// ml_flags: Flags denotando caracteristicas especiais deste metodo, tais como:
//      aceitar ou nao parametros, aceitar parametros nomeados (kwars), 
//      ser um metodo de classe, estatico, etc;
// ml_doc:  docstring desta funcao, como sera exibida para o usuario final.
static PyMethodDef modelo_methods[] = { 
    {"modelo_randomico", modelo_randomico, METH_VARARGS, modelo_randomico_doc},  
    {NULL, NULL, 0, NULL}  // sentinela para indicar o fim da passagem de metodos
};

// Definicao do modulo
// Os argumentos desta estrutura denotam o nome da extensao, a documentacao dela, 
// algumas flags, e um ponteiro para a lista de funcoes da extensao.
static struct PyModuleDef modelo_definition = { 
    PyModuleDef_HEAD_INIT,  // sempre inicie a definicao com esta flag
    "modelo",
    "Um modulo de exemplo",
    -1, 
    modelo_methods
};

// Inicializacao do modulo
// Python chama esta funcao quando um usuario importa a sua extensao. 
// E importante que esta funcao seja nomeada como PyInit_[[nome_do_seu_modulo]], 
// sendo que nome_do_seu_modulo tem exatamente o mesmo nome da entrada "name" na funcao
// setup do arquivo setup.py
PyMODINIT_FUNC PyInit_modelo(void) {
    Py_Initialize();
    return PyModule_Create(&modelo_definition);
}
